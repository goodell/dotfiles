
"colorscheme slate

"colorscheme desert
"colorscheme molokai
" I sort of like it either way (=1 or =0)
"let g:molokai_original=0
" rework the gui colors for diffs to look better in the desert/molkai schemes
"highlight       diffAdded   guifg=yellowgreen
"highlight       diffRemoved guifg=indianred
"highlight       diffSubname guifg=SkyBlue

colorscheme solarized
highlight       diffAdded   guifg=#859900
highlight       diffRemoved guifg=#dc322f
"highlight       diffSubname guifg=#

" was favorite at ANL
"set guifont=Terminus\ Medium:h14

"set guifont=ProggySquare:h11
"set guifont=Crisp:h16
"set guifont=Courier:h12
"set guifont=Monaco:h13
"set guifont=Inconsolata-dz\ for\ Powerline:h13
set guifont=Inconsolata-dz\ for\ Powerline:h15
"set guifont=Fira\ Code\ Light:h15
set macligatures

" obviously highly dependent on the above 'guifont' setting
"set lines=52
"set columns=209
set lines=66
set columns=212


set guioptions-=r
set guioptions-=R
set guioptions-=l
set guioptions-=L
set guioptions-=b
set guioptions-=T

" I prefer the console dialogs, but we can't use them because they can hang
" MacVim sometimes.  The scenario is:
" 0) set MacVim to open files from the Finder in tabs
" 1) open file "foo"
" 2) outside of vim, "touch foo"
" 3) now open a file "bar" with MacVim via the Finder
" 4) MacVim gets the focus, notices "foo" has changed and asks if it should
"    update via the console
" 5) but MacVim is also attempting to open "bar" and the focus seems to be gone
"    from the "foo" window, so you can never answer the dialog, but "bar" never
"    entirely opens either
"set guioptions+=c
set guioptions-=c

" use col -b to get rid of the nasty formatting that nroff inserts that doesn't
" agree with the lame dumb terminal builtin to gvim
map K :!man <cword> \| col -b \| less -+R<CR>

" for MacVim, see :help macvim-user-defaults
" defaults write org.vim.MacVim MMZoomBoth 1
