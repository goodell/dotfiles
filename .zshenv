##############################################################
# disable /etc/{zprofile,zshrc,zlogin,zlogout}, then source manually in
# better order
#
# Problem is that /etc/zprofile is used to source /etc/profile in ksh emulation
# mode, but: (A) zprofile is sourced after zshenv, and (B) zprofile is only run
# for login (interactive?) shells.  So we get a different environment for
# interactive and non-interactive shells, such as when performing git over ssh.
#
# We disable further sourcing of global rcfiles (from "/etc") at this point.
# /etc/zshenv is sourced before this file but contains nothing on savbu-usnic.
# So we are effectively shutting everything down from here out.
unsetopt GLOBAL_RCS

# not setting this leads to *insane* behavior where "for $asdf ; do ... ; done"
# will not autosplit $asdf into words, which is totally incompatible with common
# sense and all other bourne shell derivatives
setopt sh_word_split

# TODO need to verify if this causes path_helper problems on OS X
# (see https://github.com/sorin-ionescu/prezto/issues/381)
#[[ -e "/etc/zprofile" ]] && source /etc/zprofile
# only do the safe bits that /etc/zprofile would have done
_src_etc_profile()
{
    # source profile
    selected_profile=(
        /etc/profile.d/glib2.sh
        /etc/profile.d/lang.sh
        /etc/profile.d/man.sh
        /etc/profile.d/modules.sh
        /etc/profile.d/qt.sh
        /etc/profile.d/zz-savbu-usnic.sh
    )

    #  Make /etc/profile.d/*.sh happier, and have possible ~/.zshenv options
    #  like NOMATCH ignored.
    emulate -L ksh

    for i in ${selected_profile[@]} ; do
        if [[ -r "$i" ]] ; then
            if [[ -o INTERACTIVE ]] ; then
                . "$i"
            else
                . "$i" >/dev/null 2>&1
            fi
        fi
    done
    unset selected_profile
    unset i
}
_src_etc_profile
unset -f _src_etc_profile


# Prevent repeated sourcing of the environment, which was causing problems,
# esp. under tmux.  This also doesn't feel like the right answer, but it does
# prevent my path from becoming clogged with repeated entries and it makes RVM
# do the right thing.
if [[ "$sourced_home_zshenv" -ne 1 ]] ; then

    ##############################################################
    # basic manpath setup for later
    # get a sane default so that we don't clobber
    if [[ -z "$MANPATH" ]] ; then
        MANPATH=`manpath`
        export MANPATH
    fi

    ##############################################################
    # setup $ld_library_path as an array mirror of $LD_LIBRARY_PATH
    typeset -T 'LD_LIBRARY_PATH' 'ld_library_path' ':'

    ##############################################################
    # up the limit on number of files so that I can run bigger MPI jobs on my laptop
    ulimit -n 8192

    ulimit -c unlimited

    ##############################################################
    # also present in .zshrc to ensure we supercede any zprezto functions
    fpath=(~/.zshfpath $fpath)

    ##############################################################
    #if test $( hostname ) = "dgoodell-mac" ; then
    if [[ $( hostname ) = "DGOODELL-M-F02A"* ]] ; then

        path=(
            $HOME/bin
            $HOME/prefix/bin
            $HOME/prefix/sbin

            # homebrew paths
            /usr/local/bin
            /usr/local/sbin
            /usr/local/opt/coreutils/libexec/gnubin

            $path
        # $path normally contains:
        #/usr/bin
        #/bin
        #/usr/sbin
        #/sbin
        #/usr/local/bin
        #/opt/X11/bin
        )

        manpath=(
            $HOME/prefix/share/man
            $HOME/prefix/man
            /usr/local/share/man
            /usr/local/man

            $manpath
        # $manpath normally contains:
        #/usr/share/man
        #/usr/local/share/man
        #/opt/X11/share/man
        )
        export MANPATH

    else
        path=(
            $HOME/bin
            $path
        )
    fi
    ##############################################################

    # Settings to install ruby gems in my home dir by default.  Disabled for now
    # because it conflicts with using rvm.
    #export GEM_HOME="$HOME/.gems"
    #export GEM_PATH="$GEM_HOME"
    #export PATH="$HOME/.gems/bin:$PATH"

    export MANWIDTH=80

    export LESS='-R -i --quit-if-one-screen -X'
    export GIT_SSH=$HOME/bin/git-ssh
    export GITHUB_USER=goodell

    export EDITOR=vim

    export RUNTESTS_VERBOSE=1

    # should only affect the stock BSD "ls"
    export CLICOLORS=1

    # extended_glob messes with the way I like to refer to git commits, but
    # something (RVM, oh-my-zsh, something else?) sets it
    unsetopt extended_glob

    # $u is a helper var to quickly access the usnic btl subdir in an OMPI repo
    export u=ompi/mca/btl/usnic

    ##############################################################
    ## post-prezto tweaks:

    # needs to come after completion is loaded b/c it does a ("type compdef", which
    # changes after compinit is invoked)
    if which hub >/dev/null 2>&1 ; then
        eval "$(hub alias -s)"
    fi

    ##############################################################
    # Make sure our versions supercede any zprezto versions.  Also present in
    # .zshenv to ensure we always have it in the fpath.
    fpath=(~/.zshfpath $fpath)

    ##############################################################

    # prezto sets this to a magenta background, I prefer the default red foreground
    unset GREP_COLOR

    # I don't pay any attention to the right-hand side prompt and it interferes with copy-paste, so kill it
    RPROMPT=

    # strictly necessary still?
    bindkey -v

    # used to come earlier, but seems to be clobbered by prezto, so moving here
    bindkey -r "/"
    bindkey -r ","
    # restore more useful, non-interactive searches
    bindkey -M vicmd '?' vi-history-search-backward
    bindkey -M vicmd '/' vi-history-search-backward

    # hit F1 to debug completions
    bindkey -M viins '^[OP' _complete_debug

    # extended_glob messes with the way I like to refer to git commits, so I disable
    # it here now that we're done with it
    unsetopt extended_glob

    # https://github.com/seebi/dircolors-solarized#installation
    eval `dircolors ~/.dir_colors`

    ##############################################################
    # comes late to permit the most flexibility in controlling path precedence
    if [ -e ~/.zshenv-local ] ; then
        source ~/.zshenv-local
    fi

    export sourced_home_zshenv=1
fi ### [[ "$sourced_home_zshenv" -ne 1 ]]

# XXX DJG temporarily moved to ~/.zshenv from ~/.zshenv-local to make sure it
# runs after /etc/profile.d/modules.sh is sourced

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
# something still isn't right about this, some of the module defs end up ahead
# in the PATH, so rvm complains... try to fix that with the following line:
rvm use ruby-2.2.0 >& /dev/null
