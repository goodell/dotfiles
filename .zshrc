# disabled for now, handled by sourcing /etc/zprofile in ~/.zshenv
### expected common case, see comments in .zshenv
##if [[ ! -o GLOBAL_RCS ]] && [[ -e "/etc/zshrc" ]] ; then
##    if [[ $( hostname ) != "DGOODELL-M-F02A"* ]] ; then
##        source /etc/zshrc
##    fi
##fi

##############################################################

setopt share_history
setopt hist_ignore_all_dups
unsetopt extended_glob
unsetopt auto_cd
unsetopt auto_pushd
unsetopt auto_name_dirs
unsetopt cdablevars
unsetopt correctall
setopt flowcontrol

##############################################################
# miscellaneous functions
function vimup() {
    mydir=$(findup $1)
    if [[ $? == 0 ]] ;
    then
        vim $mydir/$1
    fi
}

# adapted from https://gist.github.com/simonjbeaumont/4672606
function tmup() {
    local IFS
    echo -n "Updating to latest tmux environment...";
    export IFS=$'\n';
    for line in $(tmux showenv -t $(tmux display -p "#S"));
    do
        if [[ "$line" == -* ]]; then
            unset $(echo $line | cut -c2-);
        elif [[ -n "$line" ]] ; then
            export "$line";
        fi
    done
    echo "Done"
}

function show_path() {
    echo ${(j:\n:)path}
}
function show_lpath() {
    echo ${(j:\n:)ld_library_path}
}

autoload -U zmv

# instructions from
# http://zsh.sourceforge.net/Doc/Release/User-Contributions.html#Accessing-On_002dLine-Help
unalias run-help
autoload run-help
HELPDIR=~/.zsh_help

##############################################################
# completions
zstyle ':completion:*' verbose true
bindkey '^R' history-incremental-search-backward

##############################################################
# aliases

if (ls -F --color=auto /dev/null >& /dev/null) ; then
    alias ls='ls -F --color=auto'
else
    alias ls='ls -F'
fi
alias sl=ls
alias grep='grep --color=auto'
alias gsfr='git svn find-rev'

alias dpaste="curl -F 'content=<-' http://dpaste.cisco.com/api/"

# has problems when getting dropped into $EDITOR is unexpected...
#alias svn=colorsvn

pdfcat () {
    gs -q -sPAPERSIZE=letter -dNOPAUSE -dBATCH -sDEVICE=pdfwrite \
        -sOutputFile=concatenated.pdf "$@"
}

# this is here for a reason... what is it?
autoload zargs

##############################################################
# FIXME sourcing this here seems to interfere with rvm somehow, but I haven't
# worked out the details yet...
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

##############################################################
## post-prezto tweaks:

# needs to come after completion is loaded b/c it does a ("type compdef", which
# changes after compinit is invoked)
### XXX DJG disabled on CEC machine for the moment b/c they don't have a
### /usr/bin/ruby installed, which screws up "hub"
###if which hub >/dev/null 2>&1 && ruby -e '' >& /dev/null ; then
###    eval "$(hub alias -s)"
###fi

##############################################################
# Make sure our versions supercede any zprezto versions.  Also present in
# .zshenv to ensure we always have it in the fpath.
fpath=(~/.zshfpath $fpath)

##############################################################

# prezto sets this to a magenta background, I prefer the default red foreground
unset GREP_COLOR

# I don't pay any attention to the right-hand side prompt and it interferes with copy-paste, so kill it
RPROMPT=

# strictly necessary still?
bindkey -v

# used to come earlier, but seems to be clobbered by prezto, so moving here
bindkey -r "/"
bindkey -r ","
# restore more useful, non-interactive searches
bindkey -M vicmd '?' vi-history-search-backward
bindkey -M vicmd '/' vi-history-search-backward

# hit F1 to debug completions
bindkey -M viins '^[OP' _complete_debug

# extended_glob messes with the way I like to refer to git commits, so I disable
# it here now that we're done with it
unsetopt extended_glob

# https://github.com/seebi/dircolors-solarized#installation
eval `dircolors ~/.dir_colors`

##############################################################
# comes late to permit the most flexibility in controlling path precedence
if [ -e ~/.zshrc-local ] ; then
    source ~/.zshrc-local
fi
