# GLOBAL_RCS should be unset here already, but let's make sure that
# /etc/zlogout can't run so that /etc/zlogout does not clear our screen
unsetopt GLOBAL_RCS
