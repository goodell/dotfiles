if exists("did_load_filetypes")
  finish
endif
augroup filetypedetect
  au! BufRead,BufNewFile *.gnuplot setfiletype gnuplot
  au! BufRead,BufNewFile *.vgout setfiletype valgrind
  au! BufRead,BufNewFile MERGE_MSG setfiletype gitcommit

  au! BufRead,BufNewFile /var/log/per-host/messages-* setfiletype messages
  au! BufRead,BufNewFile *.dmesg setfiletype messages
augroup END
