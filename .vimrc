" https://github.com/tpope/vim-pathogen
call pathogen#infect()
" seems to be fast enough and prevents me from having to remember to do this
" when I install a new bundle
call pathogen#helptags()

set nocp

" not sure if this needs to come before filetype detection is enabled to have
" the 'right' effect
syntax on
set bg=dark

filetype on
filetype plugin indent on

" some basic essentials
set expandtab
set grepprg=ack
set hlsearch
set ignorecase
set incsearch
set makeprg=make\ -w
set scrolloff=5
set shiftwidth=4
set smartcase
set softtabstop=4
set tabstop=8
set virtualedit=block
set notimeout

set colorcolumn=80
" consider this instead
"set colorcolumn=+1

set list
" old setting, fancier unicode chars enabled by sensible.vim
"set listchars=tab:>-
" another good choice is \u21b2
"let &listchars.=",extends\u21a9"
" value from sensible.vim:
"let &listchars = "tab:\u21e5 ,trail:\u2423,extends:\u21c9,precedes:\u21c7,nbsp:\u00b7"
" for Ajay and other people who have broken Unicode fonts:
"set listchars=tab:>·,trail:·,extends:·,precedes:·,nbsp:·

" NOTE temporarily disabled during Cisco bootstrapping
"" see :help fo-table
"set formatoptions+=b " don't break already long lines
"set formatoptions+=o " comment leader after 'o' or 'O'
set formatoptions+=n " format lists

if v:version >= 704
  " see :help added-7.4
  set formatoptions+=j " where it makes sense, remove comment leader when joining lines
endif

set cinoptions=c3,(0,u0,Ws " maybe also m1 (close parens like close braces)

" the vimdiff syntax highlighting ends up clashing quite severely with the
" standard underlying filetype syntax highlighting
if &diff
  syntax off
else
  syntax on
endif

" disable visual bell
set vb t_vb=

" search order: (see help 'path')
" - relative to current file
" - relative to current file, one dir up and into 'include'
" - pwd
" - relative to pwd, 'include' dir
" - relative to pwd, 'src/include' dir
" - relative to pwd, any dir
" - absolute '/usr/include'
set path=.,../include,,include,src/include,**,/usr/include

if has('persistent_undo')
  " sensible.vim sets up general persistent undo, here are my customizations
  " (except for files in '/tmp')
  au BufWritePre /tmp/* setlocal noundofile
  " don't let undo files litter the filesystem
  set undodir^=~/.vim/undo
endif

if exists('+undoreload')
  " set to negative to allow undo to work across reloads
  set undoreload=-1
endif

" which cscope search types will show up in the quickfix (compiler error)
" window
set cscopequickfix=s-,c-,d-,i-,t-,e-

" preview isn't all it's cracked up to be
"set completeopt=preview,longest,menu
set completeopt=longest,menu
set wildmode=list:longest,list:full

" makes it easier to do completions and such when in the '_build' subdir of a
" VPATH-built repo
set tags+=../tags,../TAGS

set viminfo='30,<300,/50,h,%
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
if !has('nvim')
  " restore last position, see :help last-position-jump
  set viminfo+=n~/.viminfo
endif

" I increasingly prefer line numbering...
set number


if exists("neovim_dot_app")
  source ~/.gvimrc
endif

" this doesn't seem to actually work
au BufReadCmd *.docx call zip#Browse(expand("<amatch>"))

"--------------------------------------------------------------------------------
" don't actually want this on by default
set noautoread

" makes ':set autoread' do what one would expect
au CursorHold * checktime

"--------------------------------------------------------------------------------
" VimL code looks better with 2-space indenting
autocmd FileType vim setlocal shiftwidth=2
autocmd FileType vim setlocal softtabstop=2

" local completion only for perl, that's all that ever seems to be useful anyway
autocmd FileType perl let g:SuperTabDefaultCompletionType='<C-X><C-N>'

autocmd FileType ruby setlocal shiftwidth=2
autocmd FileType ruby setlocal softtabstop=2

autocmd FileType mail setlocal spell
autocmd FileType mail setlocal tw=72

" for http://www.vim.org/scripts/script.php?script_id=3128
" (not currently installed)
"autocmd !BufRead /tmp/mutt* source ~/.mutt/address-search.vim

autocmd FileType gitcommit setlocal tw=72
autocmd FileType gitcommit setlocal spell

autocmd FileType markdown setlocal spell
" get better numbered list formatting (see ':help fo-table', option 'n')
autocmd FileType markdown setlocal autoindent
autocmd FileType markdown setlocal tw=78

" make patch reviewing a bit easier
autocmd FileType diff setlocal number

" when reading man pages, tabs, etc. are distracting from the content
autocmd FileType man setlocal nolist

"--------------------------------------------------------------------------------
" eliminate the infuriating CTRL-A/CTRL-X increment/decrement behavior
nmap <C-a> <Nul>
nmap <C-x> <Nul>

"--------------------------------------------------------------------------------
" :tjump is my normal expectation for :tag's actual behavior
map <C-]> :tjump <C-r><C-w><CR>

"--------------------------------------------------------------------------------
let g:bufExplorerShowDirectories=1
"let g:bufExplorerSortBy='name'
let g:bufExplorerSortBy='mru'

map <PageUp> :cprevious<CR>
map <PageDown> :cnext<CR>

"--------------------------------------------------------------------------------
" snippet from http://stackoverflow.com/a/14094487/158513
if !has('gui_running')
    " Compatibility for Terminal
    "let g:solarized_termtrans=1

    if (&t_Co >= 256 || $TERM == 'xterm-256color')
        " Do nothing, it handles itself.
    else
        " Make Solarized use 16 colors for Terminal support
        let g:solarized_termcolors=16
    endif
endif

colorscheme solarized

call togglebg#map("<F5>")

"--------------------------------------------------------------------------------
" these are questionable given the solarized colorscheme that is currently
" being used... leave them disabled for now
"hi Pmenu          term=reverse   ctermbg=blue ctermfg=White
"hi PmenuSel       term=reverse   ctermbg=DarkRed ctermfg=yellow
"
"hi MatchParen term=reverse cterm=reverse

" fix up the syntax highlighting when viewing diffs
highlight! link diffError Error
highlight! link diffNoEOL diffError
""highlight! link diffAdded Type
"highlight! link diffAdded NONE
"highlight       diffAdded term=underline ctermfg=2
"highlight! link diffRemoved NONE
"highlight       diffRemoved term=underline ctermfg=9
"highlight! link diffFile Identifier

"--------------------------------------------------------------------------------
" makes wiki editing easier, needs surround.vim
let @w = "s<code>"

command! FOLD setlocal foldmarker={,} | set foldcolumn=3 | set foldmethod=marker
command! UNFOLD setlocal foldmarker& foldcolumn& foldmethod& | normal! zE

let g:SuperTabRetainCompletionType=2
autocmd FileType c let g:SuperTabDefaultCompletionType="<C-X><C-O>"

" my attempt to fix spurious /^.*libtool.*:/ lines when building DAPL
" cribbed from http://bit.ly/1nQub56
autocmd FileType c let &errorformat="%-G%.%#libtool%.%#version-info%.%#,".&errorformat

" do a :ptag on the identifier under the cursor
map <C-P> <C-W>}

"--------------------------------------------------------------------------------
" permit ({...}) constructs
let c_no_curly_error=1
" c_syntax_for_h          use C syntax for *.h files, instead of C++
let c_syntax_for_h=1
"c_space_errors         trailing white space and spaces before a <Tab>
let c_space_errors=1

"--------------------------------------------------------------------------------
" latex-suite
autocmd FileType tex setlocal spell
autocmd FileType tex setlocal suffixesadd+=.bib,.tex,.cls

autocmd FileType tex syn region texZone   start="\\begin{semiverbatim}"  end="\\end{semiverbatim}\|%stopzone\>" fold

" avoid defaulting to plaintex, since I never write plain old Knuth-style TeX.
let g:tex_flavor = "latex"

" defaults:
"verbatim,comment,eq,gather,align,figure,table,thebibliography,keywords,abstract,titlepage
" comma first indicates append to default string
let g:Tex_FoldedEnvironments=',frame'
" swapped %%fakesection and section so that fakesections don't live inside of sections
"let g:Tex_FoldedSections='part,chapter,%%fakesection,section,subsection,subsubsection,paragraph'

" NOTE: disabled for now while the latex suite is not installed
"" some useful latex mappings
"augroup MyIMAPs
"    au!
"    au VimEnter * call IMAP('EFE', "\\begin{frame}\<CR>\\frametitle{<+title+>}\<CR><+contents+>\<CR>\\end{frame}", 'tex')
"    au VimEnter * call IMAP('EFF', "\\begin{frame}[fragile]\<CR>\\frametitle{<+title+>}\<CR><+contents+>\<CR>\\end{frame}", 'tex')
"augroup END

"--------------------------------------------------------------------------------
" ctrlp.vim: replacement for Command-T
let g:ctrlp_map = '<M-Space>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
" prevent ctrlp from switching to an already open buffer unless I press <c-t>
let g:ctrlp_switch_buffer = 'T'

"--------------------------------------------------------------------------------
" prevent annoying prompt all the time
let g:ycm_confirm_extra_conf = 0

" (hopefully) prevent annoying :messages prompts about missing completion info
" without disabling other useful diagnostics UI
let g:ycm_show_diagnostics_ui = 0

" actually use tag files
let g:ycm_collect_identifiers_from_tags_files = 1

" temporary debugging help
"let g:ycm_server_log_level = 'debug'

" shows less context than I thought
"let g:ycm_add_preview_to_completeopt = 1
"let g:ycm_autoclose_preview_window_after_completion = 1
"let g:ycm_autoclose_preview_window_after_insertion = 1

" *sigh* YCM clobbers the UltiSnips default trigger
let g:UltiSnipsExpandTrigger="<C-j>"
"let g:UltiSnipsListSnippets="<C-S-J>"

"--------------------------------------------------------------------------------
" NOTE: omnicppcomplete is currently disabled in favor of clang_complete
" omnicppcomplete config
" script: http://vim.sourceforge.net/scripts/script.php?script_id=1520
let g:OmniCpp_ShowPrototypeInAbbr=1

" unfortunate, but i need my 'path+=**' entry, so in order to obtain even
" reasonable performance I have to disable namespace searching entirely
" http://www.mail-archive.com/vim_use@googlegroups.com/msg09552.html
let g:OmniCpp_NamespaceSearch=0

"--------------------------------------------------------------------------------
let g:clang_complete_macros=1
" required a rebuild of MacVim in 64-bit mode to get this option working
let g:clang_complete_use_library=1
" disabled for now, gets in the way of fixing ':make' results quickly
"let g:clang_complete_copen=1

"--------------------------------------------------------------------------------
" from ':help clear-undo'
function ClearUndo()
    let old_undolevels = &undolevels
    set undolevels=-1
    exe "normal a \<BS>\<Esc>"
    let &undolevels = old_undolevels
    unlet old_undolevels
endfun

"--------------------------------------------------------------------------------
" vim easy-align: https://github.com/junegunn/vim-easy-align
vnoremap <silent> <Enter> :EasyAlign<cr>

"--------------------------------------------------------------------------------
" not necessarily expected to work without doing the 'fontpatcher' first
"let g:Powerline_symbols = 'fancy'
" 'unicode' is unfortunately ugly
"let g:Powerline_symbols = 'unicode'
"let g:Powerline_theme = 'solarized256'

"--------------------------------------------------------------------------------
" Enable git-style diff error highlighting when viewing patch files in vim.

" 'w:created' logic based on:
"   http://vim.wikia.com/wiki/Detect_window_creation_with_WinEnter
" executed at vim startup, causes the w:created assignment to happen after any
" WinEnter autocommands that are defined in the vimrc or various plugins
autocmd VimEnter * autocmd WinEnter * let w:created=1
" WinEnter doesn't fire for the initial window that vim creates
autocmd VimEnter * let w:created=1

" Vim matches are a window-specific property, so we need to add/delete them
" '\v' enables 'very magic' regex mode.  The fancy zero-width assertion at the
" front keeps from matching lines that didn't change in the diff.  The three
" cases that are caught are:
" 1) any whitespace at the end of the line
" 2) any contiguous alternation of spaces then tabs
" 3) any contiguous alternation of tabs then spaces
" There may be a more minimal regex, but this hacked together quickly enough.
let g:DJG_diffError_regex = '\v(^[-+].*)@<=(\s+$|( +\t+( *\t*)*)+|(\t+ +(\t* *)*))'
augroup DJG_diffError
  autocmd!
  " using WinEnter to handle the case of :vsplit
  " is filetype set here yet? (it can be, but it may be from an old window!)
  autocmd WinEnter *    if !exists('w:created') && !exists('w:mde') && &filetype == 'diff'
  autocmd WinEnter *      let w:mde = matchadd('diffError',g:DJG_diffError_regex)
  autocmd WinEnter *    endif
  " cover the case of bailing on a bufexplorer 'edit' session ('\beq') as well
  " as the :vnew case
  autocmd BufWinEnter * if !exists('w:mde') && &filetype == 'diff'
  autocmd BufWinEnter *   let w:mde = matchadd('diffError',g:DJG_diffError_regex)
  autocmd BufWinEnter * elseif exists('w:mde') && &filetype != 'diff'
  autocmd BufWinEnter *    call matchdelete(w:mde)
  autocmd BufWinEnter *    unlet w:mde
  autocmd BufWinEnter * endif
  " covers the case of 'setfiletype FOO' or 'setfiletype FOO' (and possibly other cases)
  autocmd FileType *    if expand('<amatch>') == 'diff' && !exists('w:mde')
  autocmd FileType *      let w:mde = matchadd('diffError',g:DJG_diffError_regex)
  autocmd FileType *    elseif exists('w:mde')
  autocmd FileType *      call matchdelete(w:mde)
  autocmd FileType *      unlet w:mde
  autocmd FileType *    endif
augroup END

"--------------------------------------------------------------------------------
" support for https://github.com/bhilburn/kernel-coding-style
nnoremap <silent> <leader>k :SetLinuxFormatting<cr>

"--------------------------------------------------------------------------------
function FnCopyMode()
  colorscheme default
  set nonumber
  set nolist
  set colorcolumn=
endfun
command! CopyMode :call FnCopyMode()

"--------------------------------------------------------------------------------
" A 'DAPL grep' helper command.  It makes it easy to grep only the relevant
" bits of the DAT/DAPL code without grepping in libusnic_direct or any of the
" non-libdaplusnic providers.  Expects to be run from the root directory of
" the dapl source tree.
command! -nargs=+ Dgrep :grep --cc --ignore-dir=dapl/usnic/usd <args> dapl/usnic dapl/udapl dapl/common dat

"--------------------------------------------------------------------------------
" view man pages in Vim (see :help find-manpage)
runtime! ftplugin/man.vim
