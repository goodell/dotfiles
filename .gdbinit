set print pretty on

add-auto-load-safe-path /lib64:/home/dgoodell/git

define offsetof_struct
    print ((char*)&((struct $arg0 *)0)->$arg1 - (char*)0)
end

define offsetof
    print ((char*)&(($arg0 *)0)->$arg1 - (char*)0)
end

define pktdump
    dump binary memory /tmp/dgoodell-pktdump.bin (char*)$arg0 (((char*)$arg0)+$arg1)
    shell od -Ax -tx1 -v /tmp/dgoodell-pktdump.bin | text2pcap - - | tshark -V -x -i -
end
document pktdump
Dump a network packet in memory with tshark.
Example usage: pktdump ADDRESS LENGTH
end

define ntoa
    set $ipv4 = $arg0
    set $val1 = ($ipv4 >>  0) & 0xff
    set $val2 = ($ipv4 >>  8) & 0xff
    set $val3 = ($ipv4 >> 16) & 0xff
    set $val4 = ($ipv4 >> 24) & 0xff
    printf "IPV4 = %u = 0x%02x.%02x.%02x.%02x = %d.%d.%d.%d (assuming NBO & LE host)\n", $ipv4, $val1, $val2, $val3, $val4, $val1, $val2, $val3, $val4
    printf "IPV4 = %u = 0x%02x.%02x.%02x.%02x = %d.%d.%d.%d (assuming HBO & LE host)\n", $ipv4, $val4, $val3, $val2, $val1, $val4, $val3, $val2, $val1
end
document ntoa
Dump an unsigned 32-bit value as a dotted-quad string.
Example usage: ntoa U32_EXPR
end
